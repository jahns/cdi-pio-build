#! /usr/bin/env bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}bash_functions"
script_dir=$(cd "$script_dir"; pwd)

HOST_GCC=${HOST_GCC-/usr/bin/gcc}
HOST_CFLAGS=${HOST_CFLAGS--pipe -O2 -Wall -Wextra -std=gnu99}
destdir=$1
SCRATCH=$2
elaborate_suffix=false
# if scratch_is_global evaluates to true, copy binary files only once
# to shared scratch area
scratch_is_global=${scratch_is_global-false}
if [[ x"${3+set}" = xset ]]; then
  libcopy=$3
fi
if [[ -z "${srun_version-}" ]]; then
  srun_version=$(/usr/bin/srun --version)
  srun_version=${srun_version#slurm }
fi


case $(version_compare "$srun_version" '21.08') in #(
  1)
    case $(version_compare "$srun_version" '20.11') in #(
      1)
        # earlier versions of slurm cannot automatically generate
        # the suffix from the step id, use a random suffix that
        # hopefully won't collide
        elaborate_suffix=:
        bcast_args=("--bcast=\"$SCRATCH/slurm_bcast_\$SLURM_JOB_ID.\$random_suffix\"")
        ;; #(
      0|2)
        # versions 20.11 of slurm and newer can already construct the
        # bcast file name themselves from a directory stem
        bcast_args=("--bcast=\"$SCRATCH/\"")
        ;;
    esac
    ;; #(
  0|2)
    set -e
    # versions 21.8 of slurm and newer can already send libraries
    bcast_args=("--bcast=\"$SCRATCH/\" \"--send-libs\"")
    unset libcopy
    ;;
esac
if [[ x${libcopy+set} = xset ]]; then
  bcast_args+=("--task-prolog=\"$SCRATCH/bcast-srun-task-prolog.\$random_suffix\"")
fi

mkdir -p "${destdir}"
if [[ x${libcopy+set} = xset ]]; then
  task_epilog_parse='|'
else
  task_epilog_parse=")
    srun_opts+=(\"\${!i}\")
    ;;
 "
fi
cat >"${destdir}/srun" <<EOF
#! /bin/bash
shopt -s extglob
set -e
srun_opts=()
prog_opts=()
prog=''
declare -i i j
STEP_NUM_NODES=\${SLURM_JOB_NUM_NODES}
for ((i=1; i <= \$# ; ++i)); do
  case \${!i} in
  -N )
    j=\$((i+1))
    srun_opts+=("\${!i}" "\${!j}")
    # skip option argument on next loop iteration
    i=\$j
    STEP_NUM_NODES=\${!j}
    ;;
  -N+([0-9]) )
    srun_opts+=("\${!i}")
    STEP_NUM_NODES=\${!i:2}
    ;;
  --nodes=* )
    srun_opts+=("\${!i}")
    STEP_NUM_NODES=\${!i#*=}
    ;;
  -A | -b | -c | -d | -D | -e | -i | -I | -J | -L | -M | -m | \\
  -n | -N | -o | -p | -q | -r | -S | -T | -t | -W | -C | -w | \\
  -x | -B | -G | --epilog)
    j=\$((i+1))
    srun_opts+=("\${!i}" "\${!j}")
    # skip option argument on next loop iteration
    i=\$j
    ;;
  --compress | -E | --preserve-env | -H | --hold | -k | --no-kill | \\
  -K | --kill-on-bad-exit | -l | --label | --multi-prog | --nice | \\
  -O | --overcommit | --overlap | --propagate | --pty | \\
  --quit-on-interrupt | -Q | --quiet | --reboot | -s | --oversubscribe | \\
  --spread-job | -u | --unbuffered | -v | --verbose | --use-min-nodes | \\
  -X | --disable-status | --contiguous | -Z | --no-allocate | --exact | \\
  --exclusive | --resv-ports | -h | --help | -V | --version)
    srun_opts+=("\${!i}")
    ;;
  --account=* | --acctg-freq=* | --bb=* | --bbf=* | --bcast=* | \\
  --begin=* | --cpus-per-task=* | --comment=* | --compress=* | \\
  --cpu-freq=* | --dependency=* | --deadline=* | --delay-boot=* | \\
  --chdir=* | --export=* | --error=* | --epilog=* | --gres=* | \\
  --gres-flags=* | --input=* | --immediate=* | --jobid=* | \\
  --job-name=* | --licenses=* | --clusters=* | --distribution=* | \\
  --mail-type=* | --mail-user=* | --mcs-label=* | --mpi=* | \\
  --ntasks=* | --nice=* | --ntasks-per-node=* | \\
  --output=* | --het-group=* | --partition=* | --power=* | \\
  --priority=* | --prolog=* | --profile=* | --propagate=* | \\
  --qos=* | --relative=* | --core-spec=* | --signal=* | \\
  --slurmd-debug=* | --switches=* | --thread-spec=* | --threads=* | --time=* | \\
  --time-min=* | --wait=* | --wckey=* | --cluster-constraint=* | \\
  --constraint=* | --mem=* | --mincpus=* | --reservation=* | \\
  --tmp=* | --nodelist=* | --exclude=* | --exclusive=* | \\
  --mem-per-cpu=* | --extra-node-info=* | --sockets-per-node=* | \\
  --cores-per-socket=* | --threads-per-core=* | --ntasks-per-core=* | \\
  --ntasks-per-socket=* | --cpu-bind=* | --hint=* | --mem-bind=* | \\
  --cpus-per-gpu=* | --gpus=* | --gpu-bind=* | --gpu-freq=* | \\
  --gpus-per-node=* | --gpus-per-socket=* | --gpus-per-task=* | \\
  --mem-per-gpu=* | --usage | -n+([0-9]) | -N+([0-9]))
    srun_opts+=("\${!i}")
    ;;
  --task-prolog=* $task_epilog_parse --task-epilog=* )
    echo "option \${!i%%=*} currently unsupported" >&2
    exit 1
    ;;
  -* )
    echo "encountered unexpected option \${!i}!" >&2
    srun_opts+=("\${!i}")
    ;;
  *)
    # non-option, should be a program or script
    if [[ "\${!i}" = /* && -x "\${!i}" ]]; then
      pathprog=\${!i}
    elif [[ "\${!i}" = */* && -x "\${!i}" ]]; then
      pathprog=\$(readlink -f "\${!i}")
    elif pathprog=\$(command -v "\${!i}"); then
      :
    elif [[ -x "\${!i}" ]]; then
      pathprog="\${!i}"
    else
      echo "Cannot determine path to executable \${!i}" >&2
      exit 1
    fi
    prog=\${!i}
    j=\$((i+1))
    prog_opts=("\${@:j}")
    break
    ;;
  esac
done
random_suffix=\$(uuidgen)
srun_opts+=(--task-epilog="$SCRATCH/cleanup.\$random_suffix")
srun_opts+=(--prolog="${destdir}/bcast-srun-prolog")
srun_opts+=($(printf -v __ " %s" "${bcast_args[@]}";printf '%s' "${__:1}"))
exec /usr/bin/srun -D "$SCRATCH" "\${srun_opts[@]}" \
  "\$pathprog" "\${prog_opts[@]}"
EOF
chmod +x "${destdir}/srun"
# Destructuring of HOST_GCC and HOST_CFLAGS is intentional here
# shellcheck disable=SC2086
${HOST_GCC} ${HOST_CFLAGS} -o "${destdir}/bcast-srun-cleanup" \
  -x c - -lslurm <<GCCEOF
#define _GNU_SOURCE
#include <dirent.h>             // opendir
#include <errno.h>              // errno
#include <limits.h>             // LONG_MAX LONG_MIN
#include <pwd.h>                // getpwuid
#include <slurm/slurm.h>        // slurm_hostlist_*
#include <stdio.h>              // fopen
#include <stdlib.h>             // getenv
#include <string.h>             // memcpy, strlen
#include <sys/types.h>          // opendir
#include <sys/utsname.h>        // uname
#include <unistd.h>             // unlink

FILE *log_fp = NULL;

static void
open_log_file(void)
{
  if (log_fp) return;
  static const char log_file_name[] = "/srun_bcast_errors.log";
  struct passwd *pw = getpwuid(getuid());
  const char *homedir = pw->pw_dir;
  size_t home_path_len = strlen(homedir);
  char log_file_fullpath[home_path_len + sizeof (log_file_name)];
  memcpy(log_file_fullpath, homedir, home_path_len);
  memcpy(log_file_fullpath + home_path_len, log_file_name,
         sizeof (log_file_name));
  log_fp = fopen(log_file_fullpath, "a");
  setlinebuf(log_fp);
}

static void
log_error(const char *op, const char *arg, const char *place,
          int op_errno)
{
  open_log_file();
  fprintf(log_fp, "failed %s(%s%s%s) %s%s%serrno=%d%s%s\\n",
          op, arg?"\"":"", arg?arg:"", arg?"\"":"",
          place?"at \"":"", place?place:"", place?"\" ":"",
          op_errno, op_errno?": ":"", op_errno?strerror(op_errno):"");
}


static long
find_node_last_local_task(void)
{
  const char *step_hostlist_env = getenv("SLURM_STEP_NODELIST"),
    *step_nnodes_env = getenv("SLURM_STEP_NUM_NODES"),
    *step_tasks_per_node_env = getenv("SLURM_STEP_TASKS_PER_NODE");
  if (!step_hostlist_env || !step_nnodes_env || !step_tasks_per_node_env)
    exit(1);
  hostlist_t step_hostlist = slurm_hostlist_create(step_hostlist_env);
  struct utsname uname_buf;
  {
    int rc = uname(&uname_buf);
    if (rc != -1) ; else {
      log_error("uname", NULL, NULL, errno);
      exit(1);
    }
  }
  int host_pos = slurm_hostlist_find(step_hostlist, uname_buf.nodename);
  slurm_hostlist_destroy(step_hostlist);
  long step_nnodes;
  {
    char *endptr;
    step_nnodes = strtol(step_nnodes_env, &endptr, 10);
    if ((errno == ERANGE
         && (step_nnodes == LONG_MAX || step_nnodes == LONG_MIN))
        || (errno != 0 && step_nnodes == 0) || endptr == step_nnodes_env) {
      log_error("strtol", step_nnodes_env, "SLURM_STEP_NUM_NODES", errno);
      exit(1);
    }
  }
  if (host_pos > step_nnodes)
    abort();
  {
    const char *tpn_str = step_tasks_per_node_env;
    char *endptr;
    long tpn_pos = -1;
    long tpn = 0;
    do {
      tpn = strtol(tpn_str, &endptr, 10);
      if ((errno == ERANGE && (tpn == LONG_MAX || tpn == LONG_MIN))
        || (errno != 0 && tpn == 0) || endptr == tpn_str) {
        log_error("strtol", tpn_str, "SLURM_STEP_TASKS_PER_NODE", errno);
        exit(1);
      }
      tpn_str = endptr;
      if (*tpn_str == '(' && tpn_str[1] == 'x') {
        tpn_str += 2;
        long tpn_nnodes = strtol(tpn_str, &endptr, 10);
        if ((errno == ERANGE && (tpn == LONG_MAX || tpn == LONG_MIN))
            || (errno != 0 && tpn == 0) || endptr == tpn_str) {
          log_error("strtol", tpn_str, "SLURM_STEP_TASKS_PER_NODE", errno);
          exit(1);
        }
        tpn_pos += tpn_nnodes;
        tpn_str = endptr;
        if (*tpn_str != ')') {
          open_log_file();
          fprintf(log_fp, "unexpected element in SLURM_STEP_TASKS_PER_NODE: %s\\n",
                  tpn_str);
          exit(1);
        }
        ++tpn_str;
      } else {
        ++tpn_pos;
        ++tpn_str;
      }
      if (*tpn_str == ',') ++tpn_str;
    } while (tpn_pos < host_pos);
    return tpn-1;
  }
}

int main(int argc, char *argv[])
{
  (void)argc;
  {
    const char *slurm_localid_env = getenv("SLURM_LOCALID");
    if (!slurm_localid_env)
      abort();
    long slurm_localid;
    {
      char *endptr;
      slurm_localid = strtol(slurm_localid_env, &endptr, 10);
      if ((errno == ERANGE
           && (slurm_localid == LONG_MAX || slurm_localid == LONG_MIN))
          || (errno != 0 && slurm_localid == 0)
          || endptr == slurm_localid_env || *endptr) {
        log_error("strtol", slurm_localid_env, "SLURM_LOCALID", errno);
        exit(1);
      }
    }
    long last_localid = find_node_last_local_task();
    if (slurm_localid != last_localid)
      return 0;
  }
  static const char js_prefix[]="$SCRATCH/slurm_bcast_",
    jslibs_suffix[]=".libs";
  const char *job_id_env = getenv("SLURM_JOB_ID"),
    *step_id_env = getenv("SLURM_STEP_ID")$(if ! $elaborate_suffix ; then
  printf '%s' ',
' \
'    *nodename_env = getenv("SLURMD_NODENAME")' ; fi);
  if (!job_id_env || !step_id_env$(if ! $elaborate_suffix ; then
  printf '%s' ' || !nodename_env' ; fi))
    abort();
  size_t job_id_env_len = strlen(job_id_env),
    step_id_env_len = strlen(step_id_env)$(if ! $elaborate_suffix ; then
  printf '%s' ',
' \
'    nodename_env_len = strlen(nodename_env)' ; fi);
  enum { uuid_slen = 36 };
  size_t argv0_len = strlen(argv[0]);
  if (argv0_len < uuid_slen)
    abort();
  const char *step_uuid = argv[0]+argv0_len-uuid_slen;
$(if $elaborate_suffix ; then
  printf '%s\n' \
    '  char fn[sizeof (js_prefix) + job_id_env_len + uuid_slen + 2' \
    '          + sizeof(jslibs_suffix)]' ;
else
  printf '%s\n' \
    '  char fn[sizeof (js_prefix) + job_id_env_len + step_id_env_len + 3' \
    '          + uuid_slen + nodename_env_len + sizeof(jslibs_suffix)]' ;
fi);
  size_t ofs = 0;
  memcpy(fn+ofs, js_prefix, sizeof(js_prefix) - 1);
  ofs += sizeof(js_prefix) - 1;
  memcpy(fn+ofs, job_id_env, job_id_env_len);
  ofs += job_id_env_len;
  fn[ofs] = '.';
  ++ofs;
$(if $elaborate_suffix ; then
  printf '%s\n' '  memcpy(fn+ofs, step_uuid,' \
    '         uuid_slen+1);' ;
else
  printf '%s;\n' \
    '  memcpy(fn+ofs, step_id_env, step_id_env_len)' \
    '  ofs+=step_id_env_len' \
    "  fn[ofs] = '_'" \
    '  ++ofs' \
    '  memcpy(fn+ofs, nodename_env, nodename_env_len+1)';
fi)
  // unlink job step binary
  if (unlink(fn))
    log_error("unlink", fn, NULL, errno);
$(if $elaborate_suffix ; then
  printf '%s;\n' '  memcpy(fn+ofs, step_id_env, step_id_env_len)' \
    '  ofs += step_id_env_len';
else
  printf '%s;\n' '  --ofs';
fi)
  memcpy(fn+ofs, jslibs_suffix, sizeof(jslibs_suffix));
  if (!chdir(fn)) {
    DIR *libdir = opendir(".");
    if (!libdir) {
      log_error("opendir", ".", fn, errno);
      exit(1);
    }
    struct dirent *de;
    while ((de = readdir(libdir))) {
      if (de->d_name[0] != '.' || (de->d_name[1] && (de->d_name[1] != '.' || de->d_name[2])))
        unlink(de->d_name);
    }
    if (errno) {
      log_error("readdir", NULL, fn, errno);
      exit(1);
    }
    if (chdir("..")) {
      log_error("chdir", "..", fn, errno);
      exit(1);
    }
    if (rmdir(fn)) {
      log_error("rmdir", fn, NULL, errno);
      exit(1);
    }
  }
  size_t scratch_len = (size_t)(strrchr(js_prefix, '/') - js_prefix) + 1;
  static const char task_prolog[] = "bcast-srun-task-prolog.";
  memcpy(fn+scratch_len, task_prolog, sizeof (task_prolog) - 1);
  ofs = scratch_len + sizeof (task_prolog) - 1;
  memcpy(fn+ofs, step_uuid, uuid_slen+1);
  if (unlink(fn))
    log_error("unlink", fn, NULL, errno);
  if (unlink(argv[0]))
    log_error("unlink", argv[0], NULL, errno);
  return 0;
}
GCCEOF

cat >"${destdir}/bcast-srun-prolog" <<EOF
#! /bin/bash
set -e
slurm_step_desc=\$($(command -v scontrol) --details show step \${SLURM_JOB_ID}.\${SLURM_STEP_ID})
STEP_NUM_NODES=\${slurm_step_desc#* Nodes=}
STEP_NUM_NODES=\${STEP_NUM_NODES%% *}
declare -a orig_cmd=()
while IFS= read -r -d '' arg; do orig_cmd+=("\$arg"); done </proc/\$PPID/cmdline
pathprog=\${orig_cmd[\${#orig_cmd[@]}-\$#]}
cleanup_dest=\${orig_cmd[\${#orig_cmd[@]}-\$#-2-${#bcast_args[@]}]#--task-epilog=}
$(if ${scratch_is_global} ; then
    printf '%s\n' 'scratch_copy=(cp -p)'
  else
   printf '%s\n' "scratch_copy=($(command -v sbcast) -j \"\${SLURM_JOB_ID}.\${SLURM_STEP_ID}\" -p)" ; \
  fi)
"\${scratch_copy[@]}" \\
  "${destdir}/bcast-srun-cleanup" \\
  "\${cleanup_dest}"
EOF
chmod +x "${destdir}/bcast-srun-prolog"
if [[ x${libcopy+set} = xset ]]; then
  if $scratch_is_global ; then
    scratch_mkdir_prep=("scratch_mkdir=($(command -v mkdir) -p)")
  else
    scratch_mkdir_prep=(
      "srun_opts=(\"\${orig_cmd[@]:1:\${#orig_cmd[@]}-\$#-3-${#bcast_args[@]}}\")"
      "srun_opt_select=(\"\${!srun_opts[@]}\")"
      "for (( i=0 ; i < \${#srun_opts[@]}; ++i )); do"
      "  case \${srun_opts[i]} in"
      '    (-D|-n|-m)'
      '      unset srun_opt_select[i] srun_opt_select[i+1]'
      '      ((++i))'
      '      ;;'
      '    (-s|--oversubscribe|--exclusive)'
      '      unset srun_opt_select[i]'
      '      ;;'
      '  esac'
      'done'
      'retained_srun_opts=()'
      "for i in \"\${!srun_opt_select[@]}\"; do"
      "  retained_srun_opts+=(\"\${srun_opts[i]}\")"
      'done'
      "scratch_mkdir=($(command -v srun) \"\${retained_srun_opts[@]}\" \\"
      "   --oversubscribe \\"
      "   -D \"$SCRATCH\" \\"
      "   -n \${STEP_NUM_NODES} -m cyclic \\"
      "   $(command -v mkdir) -p)")
  fi
  cat >>"${destdir}/bcast-srun-prolog" <<EOF
task_prolog_dest=\${orig_cmd[\${#orig_cmd[@]}-\$#-1]#--task-prolog=}
"\${scratch_copy[@]}" \\
  "${destdir}/bcast-srun-task-prolog" \\
  "\${task_prolog_dest}"
libcopy=(\$($(command -v ldd) "\$pathprog" | $(command -v sed) -n -e '/ => ${libcopy//\//\\/}/{
s@.*=> @@' -e 's/ (0x[0-9a-f]*)\$//p;}'))
if (( \${#libcopy[@]} > 0 )) ; then
$(printf '  %s\n' "${scratch_mkdir_prep[@]}")
  case \$1 in
    (*/)
      libdest="\${1}slurm_bcast_\${SLURM_JOB_ID}.\${SLURM_STEP_ID}.libs"
      ;;
    (*)
      libdest="\${1%.*}.\${SLURM_STEP_ID}.libs"
      ;;
  esac
  "\${scratch_mkdir[@]}" "\$libdest"
  for (( i=0 ; i < \${#libcopy[@]} ; ++i )); do
    "\${scratch_copy[@]}" \\
      "\${libcopy[i]}" "\$libdest/\${libcopy[i]##*/}"
  done
fi
EOF
# Destructuring of HOST_GCC and HOST_CFLAGS is intentional here
# shellcheck disable=SC2086
${HOST_GCC} ${HOST_CFLAGS} \
   -o "${destdir}/bcast-srun-task-prolog" -x c - <<GCCEOF
#include <stdlib.h>             // getenv
#include <string.h>             // memcpy
#include <sys/types.h>          // stat
#include <sys/stat.h>           // stat
#include <unistd.h>             // stat

int
main(void)
{
  const char *job_id_env = getenv("SLURM_JOB_ID"),
    *step_id_env = getenv("SLURM_STEP_ID");
  static const char libdir_prefix[] = "$SCRATCH/slurm_bcast_",
    libdir_suffix[] = ".libs",
    export_prefix[] = "export LD_LIBRARY_PATH";
  if (!job_id_env || !step_id_env)
    abort();
  size_t job_id_env_len = strlen(job_id_env),
    step_id_env_len = strlen(step_id_env);
  char buf[sizeof (export_prefix) + sizeof (libdir_prefix)
           + job_id_env_len + 1 + step_id_env_len
           + sizeof (libdir_suffix)];
  char *dn = buf + sizeof (export_prefix);
  memcpy(dn, libdir_prefix, sizeof (libdir_prefix) - 1);
  size_t ofs = sizeof (libdir_prefix) - 1;
  memcpy(dn+ofs, job_id_env, job_id_env_len);
  ofs += job_id_env_len;
  dn[ofs++] = '.';
  memcpy(dn+ofs, step_id_env, step_id_env_len);
  ofs += step_id_env_len;
  memcpy(dn+ofs, libdir_suffix, sizeof (libdir_suffix));
  ofs += sizeof (libdir_suffix);
  struct stat libdir_stat;
  int rc = stat(dn, &libdir_stat);
  if (!rc && S_ISDIR(libdir_stat.st_mode)) {
    static const char ldp_name[] = "LD_LIBRARY_PATH";
    const char *ld_lib_path = getenv(ldp_name);
    size_t ld_lib_path_len = ld_lib_path ? strlen(ld_lib_path) : 0;
    char *new_ld_lib_path;
    size_t lp_ofs = sizeof (export_prefix);
    if (ld_lib_path_len>0) {
      new_ld_lib_path
        = malloc(lp_ofs + ofs + ld_lib_path_len + 1);
      memcpy(new_ld_lib_path + lp_ofs, dn, ofs-1);
      lp_ofs += ofs-1;
      new_ld_lib_path[lp_ofs++] = ':';
      memcpy(new_ld_lib_path+lp_ofs, ld_lib_path, ld_lib_path_len+1);
      lp_ofs += ld_lib_path_len;
    } else {
      new_ld_lib_path = buf;
      lp_ofs += ofs;
    }
    memcpy(new_ld_lib_path, export_prefix, sizeof (export_prefix) - 1);
    new_ld_lib_path[sizeof (export_prefix)-1] = '=';
    write(1, new_ld_lib_path, lp_ofs);
  }
  return EXIT_SUCCESS;
}
GCCEOF
fi

# execute user-specified prolog if it exists
cat >>"${destdir}/bcast-srun-prolog" <<EOF
for ((i=\${#orig_cmd[@]}-\$#$((-3-${#bcast_args[@]})) ; i > 0 ; --i )) ; do
  case \${orig_cmd[i]} in
    (--prolog=*)
      exec "\${orig_cmd[i]#--prolog=}" "\$@"
      ;;
  esac
done
EOF
