#! /bin/bash
nvhpc_version=21.5
openmpi_version=4.1.1
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}scripts/bash_functions"
setup_tracing_and_logging \
  "$HOME/cdi-pio-build-log$(date +%Y%m%dT%H%M%S)-nvhpc-${nvhpc_version}-openmpi-${openmpi_version}.txt"
set +x
ml use "$OTHERSTAGES"
ml Stages/2020
module purge
ml NVHPC/${nvhpc_version}-GCC-10.3.0
ml OpenMPI/${openmpi_version}
ml UCX/1.9.0
ml netCDF-Fortran
ml CMake/3.18.0
# ecCodes is not directly available, load dependencies instead...
ml OpenGL/2020 libaec
ml ecCodes/2.21.0-nompi
module swap netCDF/4.7.4-serial netCDF/4.7.4
set -x
#
build="nvhpc-${EBVERSIONNVHPC}-openmpi-${EBVERSIONOPENMPI}"
# adjust here
project=highresmonsoon
# jutil exports PROJECT=/p/project/highresmonsoon SCRATCH=/p/scratch/highresmonsoon
jutil env activate -p "${project}"
basedir="/p/fastdata/slmet/slmet111/model_data/ICON/cdi-pio-libs"
if [[ ! -w "${basedir}" ]]; then
  # jutil sets this variable
  # shellcheck disable=SC2153
  basedir="${PROJECT}/$(id -un)/cdi-pio-libs"
fi
prefix="${basedir}"
# to here for other projects
builddir=$(mktemp -d "${XDG_RUNTIME_DIR}/cdi-pio-build-$(id -un)-XXXXXXX")
# setup directory for large test files
SCRATCH=$(mktemp -d "$SCRATCH/$(id -un)-cdi-pio-XXXXXXXX")
trap build_cdipio_cleanup EXIT

# create mpicc variant that calls gcc internally
mkdir -p "${basedir}/bin"
mpigcc="${basedir}/bin/mpigcc"
cat >"${mpigcc}" <<EOF
#! /bin/bash
# nvcc cannot reliably translate software with C99 features, swap it
# for gcc
export OMPI_CC=gcc
exec mpicc "\$@"
EOF
chmod +x "${mpigcc}"

# create launcher command that copies binaries to compute nodes on
#  juwels booster since may 2022 we need to fake the slurm version
#  because the system pretends it's 21.08 but indeed is not able to do
#  broadcasts itself
srun_version=20.02 \
${script_dir}scripts/create_srun_bcast.sh "${builddir}/bin" \
  /dev/shm "${builddir}"

salloc="salloc -N1 -A${project} -pdevelbooster --gres=gpu:1"
libaec_root=$(command -v aec) ; libaec_root=${libaec_root%/bin/aec}
jpeg_root=$(command -v cjpeg) ; jpeg_root=${jpeg_root%/bin/cjpeg}
jasper_root=$(command -v jasper) ; jasper_root=${jasper_root%/bin/jasper}
eccodes_root=$(command -v codes_info) ; eccodes_root=${eccodes_root%/bin/codes_info}
${script_dir}build-cdi-pio-stack.sh \
  build="${build}" \
  basedir="${basedir}" \
  libtype=shared \
  CC=mpicc FC=mpifort FCFLAGS='-g -O2 -tp=zen' CXX=mpic++ F77=mpifort \
  CFLAGS='-g -O2 -tp=zen' \
  CPPFLAGS="-DNDEBUG -I${libaec_root}/include -I${eccodes_root}/include" \
  LDFLAGS="-g" \
  MPI_LAUNCH="$builddir/bin/srun --exclusive" \
  builddir="${builddir}" \
  prefix="${prefix}/%n-%b" \
  multi_installs=: \
  --use-from-system libaec \
  --use-from-system eccodes \
  --use-from-system hdf5 \
  --use-from-system netcdf-c \
  --use-from-system pnetcdf \
  libaec_configure='CC=gcc' \
  yaxt_configure="CC=${mpigcc} 'CFLAGS=-g -O2 -march=znver2'" \
  yaxt_configure_env="${salloc}" \
  yaxt_check_env="${salloc}" \
  yaxt_extra_make_check_args="-j1" \
  ppm_configure_env="${salloc}" \
  ppm_check_env="${salloc}" \
  cdi_configure_env="${salloc}" \
  cdi_configure="'--with-eccodes=${eccodes_root}' 'LIBS=-L${jpeg_root}/lib64 -ljpeg' 'LDFLAGS=-g -Wl,--as-needed -L${eccodes_root}/lib64 -Wl,-rpath,${eccodes_root}/lib64:${jasper_root}/lib64:${jpeg_root}/lib64:${libaec_root}/lib64'" \
  cdi_check_env="${salloc}" \
  SCRATCH="$SCRATCH" \
  EXTRA_MAKE_CHECK_ARGS= \
  "$@"

