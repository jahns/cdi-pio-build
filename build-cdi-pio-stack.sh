#! /bin/bash
#_____________________________________________________________________________
#
set -eux
shopt -s extglob
# syspkg is an associative array where package keys are to be used
# from system instead of building them ourselves
declare -A syspkg packages_dl package_names package_inst \
  package_git package_git_branch package_git_commit \
  fresh_unpack package_src_root

#
# components to download:
#
packages_dl=(
  [libaec]='https://gitlab.dkrz.de/k202009/libaec/uploads/45b10e42123edd26ab7b3ad92bcf7be2/libaec-1.0.6.tar.gz'
  [hdf5]='https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.12/hdf5-1.12.2/src/hdf5-1.12.2.tar.bz2'
  [pnetcdf]='https://parallel-netcdf.github.io/Release/pnetcdf-1.12.3.tar.gz'
  [netcdf-c]='https://github.com/Unidata/netcdf-c/archive/refs/tags/v4.8.1.tar.gz'
  [netcdf-fortran]='https://github.com/Unidata/netcdf-fortran/archive/refs/tags/v4.5.4.tar.gz'
  [eccodes]='https://confluence.ecmwf.int/download/attachments/45757960/eccodes-2.26.0-Source.tar.gz'
  [yaxt]='https://swprojects.dkrz.de/redmine/attachments/download/519/yaxt-0.9.3.tar.xz'
  [ppm]='https://swprojects.dkrz.de/redmine/attachments/download/522/ppm-1.0.8.1.tar.xz'
)
#
# and those fetched from git directly:
#
package_git=([cdi]='https://gitlab.dkrz.de/mpim-sw/libcdi.git'
             #[yaxt]='https://gitlab.dkrz.de/dkrz-sw/yaxt.git'
             #[ppm]='https://gitlab.dkrz.de/jahns/ppm.git'
            )
package_git_branch=([cdi]='cdi-1.8.x-pio-merge-followup-tj20220307'
                    [yaxt]='master'
                    [ppm]='master')

unset build_cdipio_stack_print_downloads
for ((i=1; i <= $#; ++i))
do
  arg=${!i}
  case $arg in
    --use-from-system=*)
      opt_arg=${arg#*=}
      syspkg[$opt_arg]=1
      ;;
    --unset-vars=*)
      IFS=',' read -r -a temp <<< "${arg#*=}"
      unset "${temp[@]}"
      ;;
    --print-downloads)
      build_cdipio_stack_print_downloads=:
      ;;
    *=*)
      var=${arg%%=*}
      val=${arg#*=}
      typeset "$var=$val"
      ;;
    --use-from-system)
      i=$((i+1))
      opt_arg=${!i}
      syspkg[$opt_arg]=1
      ;;
  esac
done

case $0 in
  */*)
    script_dir=${0%/*}
    ;;
  *)
    script_dir=.
    ;;
esac
script_dir=$(cd "$script_dir"; pwd)
# shellcheck source=scripts/bash_functions
source "${script_dir}/scripts/bash_functions"

#
# number of concurrent build steps (i.e. argument to make -j), and
# optional argument to build makes
PAR_BUILD=${PAR_BUILD-22}
IFS=" " read -r -a EXTRA_MAKE_ARGS <<< "${EXTRA_MAKE_ARGS--j${PAR_BUILD}}"
if [[ x${EXTRA_MAKE_CHECK_ARGS+set} = xset ]]; then
  IFS=" " read -r -a EXTRA_MAKE_CHECK_ARGS <<< "${EXTRA_MAKE_CHECK_ARGS}"
else
  EXTRA_MAKE_CHECK_ARGS=("${EXTRA_MAKE_ARGS[@]}")
fi
#_____________________________________________________________________________
# tool settings
make=${MAKE-make}
if [[ $make == make ]] && ! command -v make >/dev/null 2>&1 ; then
  if command -v gmake >/dev/null 2>&1 ; then
    make=gmake
  fi
fi
build=${build-gcc}
# can also contain 'clean'
stages=${stages-'download-unpack-build-check-install'}
stages="-${stages}-"

case $build in
  impi-icc)
    # Intel MPI with Intel compiler
    CC=${CC-mpiicc}
    FC=${FC-mpiifort}
    CXX=${CXX-mpiicpc}
    ;;
  *)
    # lots of systems
    CC=${CC-mpicc}
    FC=${FC-mpifort}
    CXX=${CXX-mpic++}
    ;;
esac

AR=${AR-ar}
RANLIB=${RANLIB-ranlib}

libtype=${libtype-shared}

case $libtype in
  static)
    libsuffix=.a
    ;;
  shared|both)
    libsuffix=.so
    ;;
esac

CC_rpath_flag=${CC_rpath_flag--Wl,-rpath,}
FC_rpath_flag=${FC_rpath_flag--Wl,-rpath,}

basedir=${basedir-"${WORK-$HOME}/cdi-pio-stack"}
archivedir=${archivedir-"${basedir}/archive"}
srcdir=${srcdir-"${basedir}/src"}
builddir=${builddir-"${basedir}/build/${build}"}
case x${prefix+set}y${multi_installs+set} in
  xsetyset|xsety)
    # nothing to do, setup by caller
    :
    ;;
  xyset)
    prefix="${basedir}/opt/${build}/%pn"
    ;;
  xy)
    prefix="${basedir}/opt/${build}"
    ;;
esac

if [[ x${SCRATCH+set} = xset && \
        ( "${stages}" = *-check-* || "${stages}" = *-recheck-* ) ]]; then
  SCRATCH="${SCRATCH}/cdipio-test-files-$(id -un)-${build}"
  mkdir -p "$SCRATCH"
  trap 'rm -rf "${SCRATCH}"' EXIT
fi

# these seemingly work nicely on ClusterStore/Sonnexion, tune to
# underlying hardware or unset for defaults
NC_H5_CACHE_SIZE=${NC_H5_CACHE_SIZE-4194304}
NC_H5_CHUNK_CACHE_NELEMS=${NC_H5_CHUNK_CACHE_NELEMS-1009}

if ${build_cdipio_stack_print_downloads:-false}; then
  for package_key in ${packages_dl[@]+"${!packages_dl[@]}"}; do
    if [[ x${syspkg[$package_key]+set} != xset ]]; then
      echo "$package_key"
    fi
  done
  exit 0
fi
# derive version-qualified names
for package_key in ${packages_dl[@]+"${!packages_dl[@]}"}; do
  [[ x${syspkg[$package_key]+set} != xset ]] || continue
  package_url=${packages_dl[$package_key]}
  package_archive=${package_url##*/}
  case ${package_archive} in
    v*.tar*)
      package_name=${package_key}-${package_archive#v}
      package_name=${package_name%.tar*}
      ;;
    eccodes*)
      package_name=${package_archive%-Source.tar*}
      ;;
    *)
      package_name=${package_archive%.tar.*}
      ;;
  esac
  package_names[$package_key]=$package_name
done

declare -a args bg_pids

#_____________________________________________________________________________
# Download

case ${stages} in
  *-download-*)
    mkdir -p "$archivedir" "$srcdir" "$builddir"

    cd "$archivedir"
    bg_pids=()
    for package_key in ${packages_dl[@]+"${!packages_dl[@]}"} ; do
      [[ x${syspkg[$package_key]+set} != xset ]] || continue
      package_url=${packages_dl[$package_key]}
      package_name=${package_names[$package_key]}
      package_archive="${package_name}.tar${package_url##*/*.tar}"
      if [[ ! -e "$package_archive" ]]
      then
        wget --no-verbose -O "$package_archive" "$package_url" &
        bg_pids[$!]="downloading ${package_name} from ${package_url}"
      fi
    done
    for pid in "${!bg_pids[@]}"; do
      if ! wait "$pid" ; then
        echo "error while ${bg_pids[pid]}" >&2
        exit 1
      fi
    done
    ;;
esac

for package_key in ${packages_dl[@]+"${!packages_dl[@]}"} ; do
  [[ x${syspkg[$package_key]+set} != xset ]] || continue
  package_src_root[$package_key]="${srcdir}/${package_names[$package_key]}"
  case $package_key in
    eccodes)
      package_src_root[$package_key]+='-Source'
      ;;
  esac
done


case ${stages} in
  *-unpack-*)
    bg_pids=()
    for package_key in ${packages_dl[@]+"${!packages_dl[@]}"}; do
      [[ x${syspkg[$package_key]+set} != xset ]] || continue
      package_name=${package_names[$package_key]}
      package_url=${packages_dl[$package_key]}
      package_archive="${package_name}.tar${package_url##*/*.tar}"
      if [[ ! -d "${package_src_root[$package_key]}" ]]; then
        tar -C "$srcdir" -xf "${archivedir}/${package_archive}" &
        bg_pids[$!]="unpacking $package_name from ${archivedir}/${package_archive}"
        fresh_unpack[$package_key]=1
      fi
    done
    for pid in "${!bg_pids[@]}"; do
      if ! wait "$pid" ; then
        echo "error while ${bg_pids[pid]}" >&2
        exit 1
      fi
    done
    patch_dir="${script_dir}/patches"
    for package_key in ${packages_dl[@]+"${!packages_dl[@]}"}; do
      [[ x${syspkg[$package_key]+set} != xset ]] || continue
      package_name=${package_names[$package_key]}
      if [[ x${fresh_unpack[$package_key]+set} = xset ]]; then
        case $package_name in
          hdf5-1.12.0)
            patch -d "${package_src_root[$package_key]}" -p1 \
              <"${patch_dir}/hdf5-1.12.0-00-t_bigio-output-file-path.patch"
            ;;
          netcdf-c-4.7.4)
            patch -d "${package_src_root[$package_key]}" -p1 \
                  <"${patch_dir}/netcdf-c-pnetcdf-mpiexe.patch"
            ;;
          netcdf-c-4.8.0|netcdf-c-4.8.1)
            declare -a patches
            patches=( \
              "${patch_dir}/${package_name}-pnetcdf-mpiexe.patch")
            if [[ "$package_name" = netcdf-c-4.8.0 ]]; then
              patches+=("${patch_dir}/netcdf-c-4.8.0-h5_test.patch")
            fi
            patches+=( \
              "${patch_dir}/netcdf-c-4.8.0-pnetcdf-magic.patch" \
              "${patch_dir}/netcdf-c-4.8.0-nc_test_tst_nofill_finalization.patch" \
              )
            cat "${patches[@]}" \
              | patch -d "${package_src_root[$package_key]}" -p1 \
            ;;
          netcdf-fortran-*)
            patch -d "${package_src_root[$package_key]}" -p1 \
                  <"${patch_dir}/netcdf-fortran-v4.5.3-00.patch"
            ;;
          yaxt-0.9.0)
            patch -d "${package_src_root[$package_key]}" -p1 \
                  <"${patch_dir}/yaxt-0.9.0-pgfortran-20.patch"
            ;;
          yaxt-0.9.1|yaxt-0.9.2)
            patch -d "${package_src_root[$package_key]}" -p1 \
                  <"${patch_dir}/${package_name}-test_redist_collection_pgfortran-21.patch"
            ;;
          yaxt-0.9.3)
            patch -d "${package_src_root[$package_key]}" -p1 \
                  <"${patch_dir}/${package_name}-ftest_common_pgfortran-22.patch"
            patch -d "${package_src_root[$package_key]}" -p1 \
                  <"${patch_dir}/${package_name}-test_redist_collection_pgfortran-22.patch"
            ;;
          ppm-*)
            v_cmp_vs_1_0_8=$(version_compare \
                               "${package_name#${package_key}-}" \
                               '1.0.8')
            if [[ $v_cmp_vs_1_0_8 -lt 2 ]]; then
              patch -d "${package_src_root[$package_key]}" -p1 \
               <"${patch_dir}/ppm-1.0.8-and-older-init-omp-race-condition.patch"
            fi
            if [[ $v_cmp_vs_1_0_8 -eq 0 ]]; then
              patch -d "${package_src_root[$package_key]}" -p1 \
                <"${patch_dir}/ppm-1.0.8-cce-sparse-mask-test.patch"
            fi
            ;;
        esac
      fi
    done
    ;;
esac

case ${stages} in
  *-unpack-*-download-*|*-unpack-download-*|*-download-*-unpack-*|*-download-unpack-*)
    cd "$srcdir"
    for package_key in ${package_git[@]+"${!package_git[@]}"}; do
      [[ x${syspkg[$package_key]+set} != xset ]] || continue
      if [[ -d "$package_key" ]]; then
        current_branch=$(git -C "$package_key" rev-parse --abbrev-ref HEAD)
        if [[ "$current_branch" \
                != "${package_git_branch[$package_key]}" ]]; then
          git -C "$package_key" config remote.origin.fetch \
              '+refs/heads/*:refs/remotes/origin/*'
          git -C "$package_key" fetch origin
          git -C "$package_key" checkout -B "${package_git_branch[$package_key]}" \
              "remotes/origin/${package_git_branch[$package_key]}"
        else
          git -C "$package_key" fetch
        fi
        git -C "$package_key" rebase
      else
        git clone -b "${package_git_branch[$package_key]}" --depth=1 \
          "${package_git[$package_key]}" "$package_key"
      fi
    done
    ;;
esac

for package_key in ${package_git[@]+"${!package_git[@]}"}; do
  [[ x${syspkg[$package_key]+set} != xset ]] || continue
  package_git_commit[$package_key]=$(git -C "${srcdir}/${package_key}" log -1 --format=%H)
  package_names[$package_key]="${package_key}"
done

for package_key in ${package_git[@]+"${!package_git[@]}"} ; do
  [[ x${syspkg[$package_key]+set} != xset ]] || continue
  package_src_root[$package_key]="${srcdir}/${package_names[$package_key]}"
done


fn_pkg_prefix()
{
  local package_key=$1
  local prefix_=${prefix}
  if [[ x${multi_installs+set} = xset ]]; then
    local package_version package_name=${package_names[$package_key]}
    if [[ x${package_git_commit[$package_key]+set} = xset ]]; then
      package_version=${package_git_commit[$package_key]}
    else
      package_version=${package_name#${package_key}-}
    fi
    prefix_=${prefix_//%k/${package_key}}
    prefix_=${prefix_//%n/${package_name}}
    prefix_=${prefix_//%b/${build}}
    prefix_=${prefix_//%v/${package_version}}
  fi
  printf '%s' "$prefix_"
}

libdepend()
{
  local package_key
  if [[ x${multi_installs+set} = xset ]]; then
    for package_key in "$@" ; do
      if [[ x${package_inst[$package_key]+set} = xset ]]; then
        LDFLAGS="${LDFLAGS:+${LDFLAGS} }-L${package_inst[$package_key]}/lib"
        case $libtype in
          shared|both)
            if [[ ${CC_rpath_flag} = "${FC_rpath_flag}" ]]; then
              LDFLAGS+=" ${CC_rpath_flag}${package_inst[$package_key]}/lib"
            else
              FCFLAGS="${FCFLAGS:+${FCFLAGS} }${FC_rpath_flag}${package_inst[$package_key]}/lib"
              CFLAGS="${CFLAGS:+${CFLAGS} }${CC_rpath_flag}${package_inst[$package_key]}/lib"
            fi
            ;;
        esac
      fi
    done
  fi
}

incdepend()
{
  local package_key
  if [[ x${multi_installs+set} = xset ]]; then
    for package_key in "$@" ; do
      if [[ x${package_inst[$package_key]+set} = xset ]]; then
        CPPFLAGS="${CPPFLAGS:+$CPPFLAGS }-I${package_inst[$package_key]}/include"
      fi
    done
  fi
}

autotools_build()
{
  pushd "${builddir}"
  local package_key=$1
  local package_name=${package_names[$package_key]}
  if [[ x${package_git[$1]+set} = xset ]]; then
    local package_build="$package_key-${package_git_commit[$1]}"
  else
    local package_build=${package_name}
  fi
  local libname=$2
  local dependencies
  IFS=' ' read -r -a dependencies <<< "$3"
  shift ; shift ; shift
  local prefix_
  prefix_=$(fn_pkg_prefix "$package_key")
  package_inst[$package_key]="${prefix_}"
  local stages=${stages}
  local package_key_varname package_varname
  local as_tr_sh=(sed 'y%*+%pp%;s%[^_a-zA-Z0-9]%_%g')
  package_key_varname=$(echo "${package_key}" | "${as_tr_sh[@]}")
  package_varname=$(echo "$package_build" | "${as_tr_sh[@]}")
  for vname in package_key_varname package_varname ; do
    local package_stages_varname="${!vname}_stages"
    if [[ x${!package_stages_varname+set} = xset ]]; then
      stages="-${!package_stages_varname}-"
    fi
  done
  if [[ ! -e "${prefix_}/lib/${libname}${libsuffix}" \
          || "${stages}" = *-recheck-* ]] ; then
    mkdir -p "${package_build}"
    # todo: clear contents of build directory here, but find graceful
    # handling of incremental builds (allocation termination, source fix...)
    cd "$package_build"
    case ${stages} in
      *-build-*)
        if [[ ! -x ./config.status ]]; then
          # create configure arguments
          if [[ ${#dependencies[@]} -gt 0 && x${multi_installs+set} = xset ]]; then
            local LDFLAGS=${LDFLAGS+$LDFLAGS}
            libdepend "${dependencies[@]}"
            local CPPFLAGS=${CPPFLAGS+$CPPFLAGS}
            incdepend "${dependencies[@]}"
          fi
          declare -a args
          args=("$@")
          declare -i i
          for ((i=0 ; i < $# ; ++i)); do
            case ${args[i]} in
              (FC=*|CC=*|CFLAGS=*|FCFLAGS=*|F77=*|FFLAGS=*|CPPFLAGS=*|LIBS=*|LDFLAGS=*)
                typeset "${args[i]}"
                unset 'args[i]'
                ;;
            esac
          done
          local fflags_arg
          # we know that FFLAGS might be assigned to by user and will not
          # change the name established by convention
          # shellcheck disable=SC2153
          if [[ x${FFLAGS+set} = xset ]]; then
            fflags_arg=FFLAGS
          elif [[ x${FCFLAGS+set} = xset ]]; then
            fflags_arg=FCFLAGS
          fi
          args=("--prefix=${prefix_}"
                ${CC+"CC=$CC"} ${CFLAGS+"CFLAGS=$CFLAGS"}
                ${FC+"FC=$FC"} ${FCFLAGS+"FCFLAGS=$FCFLAGS"}
                "F77=${F77-$FC}"
                ${!fflags_arg+"FFLAGS=${!fflags_arg}"}
                ${CPPFLAGS+"CPPFLAGS=$CPPFLAGS"}
                ${LIBS+"LIBS=$LIBS"}
                ${LDFLAGS+"LDFLAGS=$LDFLAGS"}
                ${args[@]+"${args[@]}"})
          local package_extra_flags_varname_configure
          package_extra_flags_varname_configure="${package_key_varname}_configure"
          # the potential to incorporate internal state is intentional here
          # shellcheck disable=SC1001,SC2086
          eval args\+\=\(${!package_extra_flags_varname_configure-}\)
          package_extra_flags_varname_configure="${package_varname}_configure"
          # the potential to incorporate internal state is intentional here
          # shellcheck disable=SC1001,SC2086
          eval args\+\=\(${!package_extra_flags_varname_configure-}\)
          local package_configure_env="${package_varname}_configure_env"
          if [[ x${!package_configure_env+set} != xset ]]; then
            package_configure_env="${package_key_varname}_configure_env"
          fi
          case $libtype in
            shared)
              args+=(--disable-static)
              ;;
            static)
              args+=(--disable-shared)
              ;;
          esac
          # inspect args in reverse to eliminate redundant arguments
          declare -A seen_arg_vars
          for (( i=${#args[@]}-1 ; i >= 0 ; --i )); do
            case ${args[i]} in
              ([_a-zA-Z]*([_a-zA-Z0-9])=*)
                var=${args[i]%%=*}
                val=${args[i]#*=}
                if [[ x${seen_arg_vars[$var]+set} = xset ]]; then
                  unset 'args[i]'
                fi
                seen_arg_vars[$var]=$val
                ;;
            esac
          done
          unset seen_arg_vars
          if [[ ! -x "${package_src_root[$package_key]}/configure" ]]; then
            pushd "${package_src_root[$package_key]}"
            if [[ -x scripts/reconfigure ]]; then
              scripts/reconfigure
            else
              autoreconf -i
            fi
            if [[ -x scripts/recreate-testsuite.sh ]]; then
              scripts/recreate-testsuite.sh
            fi
            popd
          fi
          ${!package_configure_env-} \
            "${package_src_root[$package_key]}/configure" \
            ${args[@]+"${args[@]}"}
        fi
        ${make} "${EXTRA_MAKE_ARGS[@]}"
        ;;
    esac
    case ${stages} in
      *-check-*|*-recheck-*)
        local package_check_env="${package_varname}_check_env"
        if [[ x${!package_check_env+set} != xset ]]; then
          package_check_env="${package_key_varname}_check_env"
        fi
        local EXTRA_MAKE_CHECK_ARGS=("${EXTRA_MAKE_CHECK_ARGS[@]}")
        for vname in package_key_varname package_varname ; do
          local package_extra_make_check_args_varname="${!vname}_extra_make_check_args"
          if [[ x${!package_extra_make_check_args_varname+set} = xset ]]; then
            declare -a build_cdipio_stack_array_temp
            IFS=" " read -r -a build_cdipio_stack_array_temp \
               <<< "${!package_extra_make_check_args_varname}"
            EXTRA_MAKE_CHECK_ARGS+=("${build_cdipio_stack_array_temp[@]}")
          fi
        done
        ${!package_check_env-} \
          ${make} "${EXTRA_MAKE_CHECK_ARGS[@]}" check
        ;;
    esac
    case ${stages} in
      *-install-*)
      ${make} "${EXTRA_MAKE_ARGS[@]}" install
      ;;
    esac
    case $stages in
      *-clean-*)
        ${make} "${EXTRA_MAKE_ARGS[@]}" clean
        ;;
    esac
  fi
  popd
}


if [[ x${multi_installs+set} = x ]]; then
  package_inst[_]=$prefix
  multi_installs=:
  incdepend _
  libdepend _
  unset multi_installs
fi

cd "${builddir}"
#_____________________________________________________________________________
# Building libaec which is a prerequisite
[[ x${syspkg[libaec]+set} = xset ]] || \
  autotools_build libaec libaec ""

#_____________________________________________________________________________
# Building with Parallel I/O Support

# For parallel I/O to work, HDF5 must be installed with
# --enable-parallel, and an MPI library (and related libraries) must
# be made available to the HDF5 configure. This can be accomplished
# with an mpicc wrapper script.
#
# The following works from the top-level HDF5 source directory to build
# HDF5 with parallel I/O:
if [[ x${syspkg[hdf5]+set} != xset ]]; then
  if [[ x${SCRATCH+set} = xset ]]; then
    export HDF5_PARAPREFIX="${SCRATCH}"
  fi
  autotools_build hdf5 libhdf5 libaec \
  --disable-fortran --disable-cxx \
  --enable-threadsafe --enable-unsupported \
  --enable-parallel --with-default-api-version=v110 \
  --with-szlib${package_inst[libaec]+="${package_inst[libaec]}"} \
  ${MPI_LAUNCH+RUNPARALLEL="$MPI_LAUNCH -n \$\${NPROCS:=6}"}
fi

# To enable parallel I/O support for classic netCDF files, i.e. CDF-1,
# 2 and 5 formats, PnetCDF library must also be installed.
[[ x${syspkg[pnetcdf]+set} = xset ]] || \
  autotools_build pnetcdf libpnetcdf "" \
  --disable-fortran --disable-cxx --enable-shared \
  --enable-large-single-req \
  --enable-large-file-test \
  ${SCRATCH+TESTOUTDIR="${SCRATCH}"} \
  ${MPI_LAUNCH+"TESTMPIRUN=$MPI_LAUNCH"} \
  ${MPI_LAUNCH+"TESTSEQRUN=$MPI_LAUNCH -n1"}


if [[ x${syspkg[netcdf-c]+set} != xset ]]; then
  autotools_build netcdf-c libnetcdf "hdf5 pnetcdf"\
    --enable-netcdf4 --disable-dap \
    ${NC_H5_CACHE_SIZE+--with-chunk-cache-size=${NC_H5_CACHE_SIZE}} \
    ${NC_H5_CHUNK_CACHE_NELEMS+--with-chunk-cache-nelems=${NC_H5_CHUNK_CACHE_NELEMS}} \
    --enable-pnetcdf \
    --enable-parallel-tests --enable-parallel \
    --enable-large-file-tests \
    ${SCRATCH+--with-temp-large="${SCRATCH}"} \
    ${MPI_LAUNCH+--with-mpiexec="$MPI_LAUNCH"}
  nc_config="$(fn_pkg_prefix netcdf-c)/bin/nc-config"
else
  nc_config=${nc_config-$(command -v nc-config)}
fi

function netcdf_fortran_build()
{
  local args=() LIBS
  if [[ $libtype = static ]]; then
    LIBS="-lpnetcdf -lhdf5_hl -lhdf5 -lz -lsz -laec${LIBS+ $LIBS}"
  fi
  # netcdf-c 4.6.2 up to 4.8.0 inclusive suffer from a bug in
  # nc_create_par_fortran and nc_open_par_fortran when built with
  # cmake for OpenMPI
  # see https://github.com/Unidata/netcdf-c/pull/1957 for a description
  if path_mpicc=$(command -v mpicc) ; then
    mpibindir=${path_mpicc%/mpicc}
    netcdf_c_version=$(${nc_config} --version)
    netcdf_c_version=${netcdf_c_version#netCDF }
    case "$(version_compare "$netcdf_c_version" 4.6.2)$(version_compare "$netcdf_c_version" 4.8.1)" in
      21|01)
        local netcdf_c_libdir
        netcdf_c_libdir=$(${nc_config} --libdir)
        if [[ -d "${netcdf_c_libdir}/cmake" \
	        && -x "${mpibindir}/ompi_info" ]]; then
          local __="netcdf-c-4.6.2-to-4.8.0-opencreate_par"
          local CC_PIC_FLAGS_
          if [[ $libtype != static ]]; then
            CC_PIC_FLAGS_=${CC_PIC_FLAGS--fPIC}
          else
            unset CC_PIC_FLAGS_
          fi
          # it's conventional that CFLAGS is some sort of array, in
          # a little broken, adorably naive way from a bygone era
          # shellcheck disable=SC2086,SC2046
          ${CC} ${CFLAGS-} ${CC_PIC_FLAGS_-} $(${nc_config} --cflags) -c \
	        -o "${builddir}/${__}.o" "${script_dir}/patches/${__}.c"
	  ${AR} rcu "${builddir}/libncf_override.a" \
                "${builddir}/${__}.o"
          ${RANLIB} "${builddir}/libncf_override.a"
          args+=("ac_cv_search_nc_open=${builddir}/libncf_override.a -lnetcdf")
        fi
        ;;
    esac
  fi
  autotools_build netcdf-fortran libnetcdff netcdf-c \
    ${args[@]+"${args[@]}"}
}

# build netcdf fortran interface
if [[ x${syspkg[netcdf-fortran]+set} != xset && "$stages" = *-@(build|check|recheck)-* ]]; then
  netcdf_fortran_build
fi


if [[ x${syspkg[eccodes]+set} != xset ]]; then
  prefix_=$(fn_pkg_prefix eccodes)
  package_inst[eccodes]="${prefix_}"
  if [[ ! -e "${prefix_}/lib/libeccodes${libsuffix}" \
    || "${stages}" = *-recheck-* ]]
  then
    package_name=${package_names[eccodes]}
    mkdir -p "${package_name%-Source}"
    pushd "${package_name%-Source}"
    if [[ ! -e "${prefix_}/lib/libeccodes${libsuffix}" ]]; then
      case ${stages} in
        *-build-*)
          # the potential to incorporate internal state is intentional here
          # shellcheck disable=SC2086
          eval args=\(${CMAKE_EXTRA_ARGS-}\)
          if [[ $libtype = static ]]; then
            args+=("-DBUILD_SHARED_LIBS=OFF")
          elif [[ $libtype = shared ]]; then
            args+=("-DBUILD_SHARED_LIBS=ON")
          elif [[ $libtype = both ]]; then
            args+=("-DBUILD_SHARED_LIBS=BOTH")
          fi
          if [[ x${syspkg[libaec]+set} != xset ]]; then
            if [[ x${package_inst[libaec]+set} = xset ]]; then
              args+=("-DAEC_INCLUDE_DIR=${package_inst[libaec]}/include"
                     "-DAEC_LIBRARY=${package_inst[libaec]}/lib/libaec${libsuffix}")
            else
              args+=("-DAEC_INCLUDE_DIR=${prefix}/include"
                     "-DAEC_LIBRARY=${prefix}/lib/libaec${libsuffix}")
            fi
          fi
          cmake \
            "${package_src_root[eccodes]}" \
            -DCMAKE_INSTALL_PREFIX="$prefix_" \
            -DENABLE_PYTHON=OFF -DENABLE_NETCDF=OFF \
            -DENABLE_AEC=ON -DCMAKE_C_COMPILER="$CC" \
            -DCMAKE_Fortran_COMPILER="$FC" \
            -DCMAKE_INSTALL_LIBDIR=lib \
            ${FCFLAGS+-DCMAKE_Fortran_FLAGS="${FCFLAGS}"} \
            ${CFLAGS+-DCMAKE_C_FLAGS_RELEASE="${CFLAGS}"} \
            -DENABLE_EXAMPLES=OFF \
            ${args[@]+"${args[@]}"}
          ${make} "${EXTRA_MAKE_ARGS[@]}"
          ;;
      esac
    fi
    case ${stages} in
      *-check-*|*-recheck-*)
        ${make} ARGS="${EXTRA_MAKE_ARGS[*]}" test
        ;;
    esac
    if [[ ! -e "${prefix_}/lib/libeccodes${libsuffix}" ]]; then
      case ${stages} in
        *-install-*)
          ${make} "${EXTRA_MAKE_ARGS[@]}" install
          ;;
      esac
    fi
    case ${stages} in
      *-clean-*)
        ${make} clean
        ;;
    esac
    popd
  fi
fi

if [[ x${syspkg[yaxt]+set} != xset ]]; then
  autotools_build yaxt libyaxt '' \
    --with-idxtype=long \
    ${MPI_LAUNCH+MPI_LAUNCH="$MPI_LAUNCH"}
fi

if [[ x${syspkg[ppm]+set} != xset ]]; then
  autotools_build ppm libscalesppm 'netcdf-fortran' \
    ${MPI_LAUNCH+MPI_LAUNCH="$MPI_LAUNCH"}
fi

if [[ x${syspkg[cdi]+set} != xset ]]; then
  if [[ x${multi_installs+set} = xset ]]; then
    for package_key in yaxt ppm eccodes; do
      if [[ x${package_inst[$package_key]+set} = xset ]]; then
        PKG_CONFIG_PATH+="${PKG_CONFIG_PATH+:}${package_inst[$package_key]}/lib/pkgconfig"
      fi
    done
  else
    PKG_CONFIG_PATH+="${PKG_CONFIG_PATH+:}${prefix}/lib/pkgconfig"
  fi
  export PKG_CONFIG_PATH
  args=()
  deps='eccodes netcdf-c yaxt ppm'
  if [[ x${package_inst[libaec]+set} = xset ]]; then
    deps+=' libaec'
  fi
  if [[ $libtype = static ]]; then
    # because libeccodes.a does not have a corresponding libeccodes.la,
    # prevent reordering by substitution
    if [[ x${syspkg[eccodes]+set} != xset ]]; then
      eccodes_libs=$(pkg-config eccodes --libs --static \
        | sed -e "s@ -leccodes @ ${package_inst[eccodes]}/lib/libeccodes.a @")
    else
      eccodes_libs=$(pkg-config eccodes --libs)
    fi
    if [[ x$($nc_config --has-pnetcdf) = xyes ]]; then
      LIBS="${LIBS+$LIBS }-lpnetcdf"
    fi
    if [[ x$($nc_config --has-hdf5) = xyes ]]; then
      LIBS="${LIBS+$LIBS }-lhdf5_hl -lhdf5"
    fi
    if [[ x${syspkg[libaec]+set} != xset ]]; then
      eccodes_libs+=' -lsz -laec'
    fi
    args+=("LIBS=${LIBS+$LIBS }$eccodes_libs -lrt -lz")
  fi
  if [[ x${SCRATCH+set} = xset ]]; then
    export CDI_TEST_PREFIX="${SCRATCH}/cdi/example" \
      CDI_PIO_TEST_DIR="${SCRATCH}/cdi"
  fi
  if [[ x${package_inst[netcdf-c]+set} = xset ]]; then
    args+=("--with-netcdf=${package_inst[netcdf-c]}")
  else
    args+=(--with-netcdf)
  fi
  autotools_build cdi libcdipio "$deps" \
    --enable-mpi \
    --disable-hirlam-extensions \
    --with-eccodes=yes \
    --enable-iso-c-interface \
    ${MPI_LAUNCH+MPI_LAUNCH="$MPI_LAUNCH"} \
    ${CXX+"CXX=$CXX"} \
    ${args[@]+"${args[@]}"}
fi
