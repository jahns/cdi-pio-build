#! /bin/bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}scripts/bash_functions"
module load git/2.31.1-gcc-11.2.0 \
  patch/2.7.6-gcc-11.2.0 \

module load intel-oneapi-compilers/2022.0.1-gcc-11.2.0 \
  intel-oneapi-mpi/2021.5.0-intel-2021.5.0 \
  libaec/1.0.5-intel-2021.5.0 \
  eccodes/2.21.0-intel-2021.5.0 \
  parallel-netcdf/1.12.2-intel-oneapi-mpi-2021.5.0-intel-2021.5.0 \
  hdf5/1.12.1-intel-oneapi-mpi-2021.5.0-intel-2021.5.0 \
  netcdf-c/4.8.1-intel-oneapi-mpi-2021.5.0-intel-2021.5.0 \
  netcdf-fortran/4.5.3-intel-oneapi-mpi-2021.5.0-intel-2021.5.0

INTEL_VERSION=$(ifort -V 2>&1 \
  | sed -n -e '/Version/{' -e 's/.*Version \([0-9][0-9.]*\) .*/\1/;p;}')
I_MPI_VERSION=$(mpicc -dM -E - <<EOF 2>/dev/null | sed -n -e '/I_MPI_VERSION/{' -e 's/^#define I_MPI_VERSION "\(.*\)"/\1/;p;}'
#include <mpi.h>
EOF
)
build="icc-${INTEL_VERSION}-impi-${I_MPI_VERSION}"

setup_tracing_and_logging \
  "$HOME/cdi-pio-build-log$(date +%Y%m%dT%H%M%S)-${build}.txt"
set -x
builddir=${builddir-$(mktemp -d "/dev/shm/cdi-pio-build-$(id -un)-XXXXXXX")}
# should be the following, but that directory is not currently available
#SCRATCH="/lustre/scratch/${USER:0:1}/${USER}/cdi-pio-test-files-${build}"
# use dir in $HOME instead
SCRATCH="${HOME}/cdi-pio-test-files-${build}"
mkdir -p "$SCRATCH"
lfs setstripe --stripe-count 8 "$SCRATCH"
trap build_cdipio_cleanup EXIT

libaec_root=$(command -v aec) ; libaec_root=${libaec_root%/bin/aec}
eccodes_root=$(command -v codes_info) ; eccodes_root=${eccodes_root%/bin/codes_info}
netcdf_fortran_root=$(command -v nf-config) ; \
  netcdf_fortran_root=${netcdf_fortran_root%/bin/nf-config}
nff_rpath=${netcdf_fortran_root}/lib
netcdf_c_root=$(command -v nc-config) ; \
  netcdf_c_root=${netcdf_c_root%/bin/nc-config}
nfc_rpath=${netcdf_c_root}/lib
nff_libs="$(nf-config --flibs) -Wl,-rpath,${nff_rpath}"
COMMONFLAGS='-O2 -g -march=core-avx2 -mtune=core-avx2'
CFLAGS=${CFLAGS-$COMMONFLAGS}
CPPFLAGS=${CPPFLAGS-'-DNDEBUG'}
FCFLAGS=${FCFLAGS-$COMMONFLAGS}
LDFLAGS=${LDFLAGS--g -Wl,--as-needed}

${script_dir}build-cdi-pio-stack.sh \
  build="${build}" \
  multi_installs=: \
  prefix="${HOME}/cdi-pio-stack/sw/%b/%k/%v" \
  basedir="$HOME/cdi-pio-build" \
  srcdir="$HOME/cdi-pio-stack/src" \
  archivedir="$HOME/cdi-pio-stack/archive" \
  builddir="$builddir" \
  CC=mpiicc FC=mpiifort F77=mpiifort CXX=mpiicpc \
  CFLAGS="$CFLAGS" \
  FCFLAGS="$FCFLAGS" \
  LDFLAGS="$LDFLAGS" \
  SCRATCH="$SCRATCH" \
  --use-from-system libaec \
  --use-from-system eccodes \
  --use-from-system hdf5 \
  --use-from-system netcdf-c \
  --use-from-system netcdf-fortran \
  --use-from-system pnetcdf \
  ppm_configure="CFLAGS='$CFLAGS -qopenmp' FCFLAGS='$FCFLAGS -qopenmp -I${netcdf_fortran_root}/include' LIBS='${nff_libs}'" \
  cdi_configure="CFLAGS='$CFLAGS' FCFLAGS='$FCFLAGS' LIBS='${nff_libs} $(nc-config --libs) -Wl,-rpath,${nfc_rpath}' LDFLAGS='$LDFLAGS -L${eccodes_root}/lib64 -Wl,-rpath,${eccodes_root}/lib64'" \
  "$@"
