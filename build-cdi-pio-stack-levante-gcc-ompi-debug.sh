#! /bin/bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
COMMONFLAGS='-O0 -g -march=core-avx2 -mtune=core-avx2'
CFLAGS="$COMMONFLAGS" \
FCFLAGS="$COMMONFLAGS" \
LDFLAGS='-g -Wl,--as-needed' \
CPPFLAGS='' \
${script_dir}build-cdi-pio-stack-levante-gcc-ompi.sh \
  prefix="${HOME}/cdi-pio-stack/sw/%b/%k/%v-debug" \
  "$@"
