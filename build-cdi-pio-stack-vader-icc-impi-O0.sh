#! /bin/bash
set -eux
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
${script_dir}build-cdi-pio-stack-vader-icc-impi.sh \
  prefix="${HOME}/cdi-pio-stack/sw/%b/%k/%v-debug" \
  CFLAGS='-O0 -g' FCFLAGS='-O0 -g' \
  hdf5_1_12_0_configure="CFLAGS='-O0 -g'" \
  "$@"
