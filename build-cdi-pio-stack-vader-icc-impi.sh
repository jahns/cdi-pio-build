#! /bin/bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}scripts/bash_functions"

module load intel-oneapi-compilers intel-oneapi-mpi cmake

INTEL_VERSION=$(ifort -V 2>&1 \
  | sed -n -e '/Version/{' -e 's/.*Version \([0-9][0-9.]*\) .*/\1/;p;}')
I_MPI_VERSION=$(mpicc -dM -E - <<EOF 2>/dev/null | sed -n -e '/I_MPI_VERSION/{' -e 's/^#define I_MPI_VERSION "\(.*\)"/\1/;p;}'
#include <mpi.h>
EOF
)
build="icc-${INTEL_VERSION}-impi-${I_MPI_VERSION}"

setup_tracing_and_logging \
  "$HOME/cdi-pio-build-log$(date +%Y%m%dT%H%M%S)-${build}.txt"

builddir=$(mktemp -d "/dev/shm/cdi-pio-build-$(id -un)-XXXXXXX")
SCRATCH="/mnt/lustre01/scratch/${USER:0:1}/${USER}/cdi-pio-test-files-${build}"
mkdir -p "$SCRATCH"
lfs setstripe --stripe-count 8 "$SCRATCH"
trap build_cdipio_cleanup EXIT
${script_dir}build-cdi-pio-stack.sh \
  build="${build}" \
  multi_installs=: \
  prefix="${HOME}/cdi-pio-stack/sw/%b/%k/%v" \
  builddir="$builddir" \
  CC=mpiicc FC=mpiifort F77=mpiifort CXX=mpiicpc \
  CFLAGS='-O2 -g -march=core-avx2 -mtune=core-avx2' \
  FCFLAGS='-O2 -g -march=core-avx2 -mtune=core-avx2' \
  SCRATCH="$SCRATCH" \
  hdf5_1_12_0_configure="CFLAGS='-O2 -g'" \
  hdf5_1_12_1_configure="CFLAGS='-O2 -g'" \
  "$@"
