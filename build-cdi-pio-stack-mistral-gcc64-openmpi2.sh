#! /bin/bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}scripts/bash_functions"

module load cmake/3.17.1-gcc-9.1.0 gcc/6.4.0 \
  openmpi/2.0.2p2_hpcx-gcc64 \
  autoconf/2.69 automake/1.14.1 libtool/2.4.6
set -x

#ompi_version=$(ompi_info --version | sed -n -e '1s/Open MPI v//;1p')
# use a hard-coded value here because the local patch level is not
# reported by ompi_info
ompi_version=2.0.2p2
gcc_version=$(gcc --version | sed -n -e '1s/gcc \(.*\) //;1p')
build="gcc-${gcc_version}-openmpi-${ompi_version}"

setup_tracing_and_logging \
  "$HOME/cdi-pio-build-log$(date +%Y%m%dT%H%M%S)-${build}.txt"


nf_config='/sw/rhel6-x64/netcdf/netcdf_fortran-4.4.3-parallel-openmpi2-gcc64/bin/nf-config'
netcdf_dir=$(${nf_config} --flibs | sed 's@.*-L\([^ ]*netcdf_c[^/ ]*\)/lib .*@\1@')
if [[ ! -x "${netcdf_dir}/bin/nc-config" ]]; then
  echo "Cannot find nc-config!" >&2
  exit 1
fi
eccodes_root='/sw/rhel6-x64/eccodes/eccodes-2.22.0-gcc64'
builddir=$(mktemp -d "/dev/shm/cdi-pio-build-$(id -un)-XXXXXXX")
SCRATCH="/scratch/${USER:0:1}/${USER}/cdi-pio-test-files-${HOSTNAME}-$$"
mkdir -p "$SCRATCH"
lfs setstripe --stripe-count 8 "$SCRATCH"
trap build_cdipio_cleanup EXIT
export PATH="${netcdf_dir}/bin:$PATH"
${script_dir}build-cdi-pio-stack.sh \
  build="${build}" \
  builddir="$builddir" \
  multi_installs=: \
  prefix="/work/k20200/$(id -un)/sw/%k/%v-%b" \
  CPPFLAGS="-DNODEBUG $($nf_config --cflags) -I${eccodes_root}/include" \
  FCFLAGS="-g -O2 -march=core-avx2 -mtune=core-avx2 $($nf_config --fflags)" \
  CFLAGS="-g -O2 -march=core-avx2 -mtune=core-avx2" \
  LDFLAGS="-L${eccodes_root}/lib64 -Wl,-rpath,${eccodes_root}/lib64" \
  LIBS="-Wl,--as-needed $($nf_config --flibs) -leccodes -lssl -lcrypto" \
  SCRATCH="$SCRATCH" \
  --use-from-system libaec \
  --use-from-system hdf5 \
  --use-from-system pnetcdf \
  --use-from-system netcdf-c \
  --use-from-system netcdf-fortran \
  --use-from-system eccodes \
  "$@"
