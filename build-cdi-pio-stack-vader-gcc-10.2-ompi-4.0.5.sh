#! /bin/bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}scripts/bash_functions"

module load cmake gcc/10.2.0 openmpi/4.0.5-gcc-10.2.0

ompi_version=$(ompi_info --version | sed -n -e '1s/Open MPI v//;1p')
gcc_version=$(gcc --version | sed -n -e '1s/gcc \(.*\) //;1p')
build="gcc-${gcc_version}-openmpi-${ompi_version}"

setup_tracing_and_logging \
  "$HOME/cdi-pio-build-log$(date +%Y%m%dT%H%M%S)-${build}.txt"

builddir=$(mktemp -d "/dev/shm/cdi-pio-build-$(id -un)-XXXXXXX")
SCRATCH="/mnt/lustre01/scratch/${USER:0:1}/${USER}/cdi-pio-test-files-${build}"
mkdir -p "$SCRATCH"
lfs setstripe --stripe-count 8 "$SCRATCH"
trap build_cdipio_cleanup EXIT
OMPI_MCA_io=ompio \
${script_dir}build-cdi-pio-stack.sh \
  build="${build}" \
  multi_installs=: \
  prefix="${HOME}/cdi-pio-stack/sw/%b/%k/%v" \
  builddir="$builddir" \
  CC=mpicc FC=mpifort F77=mpifort CXX=mpic++ \
  CFLAGS='-g -O2 -march=znver2 -pipe' FCFLAGS='-g -O2 -march=znver2 -pipe' \
  FFLAGS='-g -O2 -march=znver2 -pipe' \
  netcdf_fortran_4_5_3_configure="FCFLAGS='-g -O2 -march=znver2 -fallow-argument-mismatch -pipe' FFLAGS='-g -O2 -march=znver2 -fallow-argument-mismatch -pipe'" \
  SCRATCH="$SCRATCH" \
  MPI_LAUNCH="/usr/bin/srun" \
  "$@"
