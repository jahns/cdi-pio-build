#! /bin/bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}scripts/bash_functions"

shopt -s extglob
module load cmake/3.17.1-gcc-9.1.0 gcc/6.4.0 nag/6.2 \
  openmpi/2.0.2p2_hpcx-nag62 \
  autoconf/2.69 automake/1.14.1 libtool/2.4.6
set -x

#ompi_version=$(ompi_info --version | sed -n -e '1s/Open MPI v//;1p')
# use a hard-coded value here because the local patch level is not
# reported by ompi_info
ompi_version=2.0.2p2
nag_version=$(nagfor -V 2>&1 \
  | sed -n -e '/NAG Fortran Compiler Release/{' -e 's/.*Release \([0-9][0-9.]*\).*/\1/;p;}')
nag_version=62
build="nag${nag_version}-openmpi-${ompi_version}"

setup_tracing_and_logging \
  "$HOME/cdi-pio-build-log$(date +%Y%m%dT%H%M%S)-${build}.txt"


nf_config='/sw/rhel6-x64/netcdf/netcdf_fortran-4.4.4-parallel-openmpi2-nag62/bin/nf-config'
netcdf_fortran_LIBS=" $($nf_config --flibs) "
if [[ ${netcdf_fortran_LIBS} =~ \ -L([^ ]*netcdf_c[^/ ]*)/lib\  ]]; then
  netcdf_dir=${BASH_REMATCH[1]}
else
  echo "Cannot find netcdf library directory!" >&2
  exit 1
fi
if [[ ! -x "${netcdf_dir}/bin/nc-config" ]]; then
  echo "Cannot find nc-config!" >&2
  exit 1
fi
eccodes_root='/sw/rhel6-x64/eccodes/eccodes-2.22.0-gcc64'
builddir=$(mktemp -d "/dev/shm/cdi-pio-build-$(id -un)-XXXXXXX")
SCRATCH="/scratch/${USER:0:1}/${USER}/cdi-pio-test-files-${HOSTNAME}-$$"
mkdir -p "$SCRATCH"
lfs setstripe --stripe-count 8 "$SCRATCH"
trap build_cdipio_cleanup EXIT
export PATH="${netcdf_dir}/bin:$PATH"
netcdf_fortran_LIBS=${netcdf_fortran_LIBS// -Wl,-Wl,,-rpath,\// -Wl,-Wl,,-rpath,,\/}
# because this is static netcdf-fortran library, all that depends on
# it must be static too. For that reason we rather compile ppm without
# netcdf dump support
netcdf_fortran_LIBS=${netcdf_fortran_LIBS# }
read -r -a netcdf_fortran_LIBS <<< "${netcdf_fortran_LIBS}-Wl,-Wl,,-rpath,,${netcdf_dir}/lib"
netcdf_fortran_LIBS_FC=()
netcdf_fortran_LIBS_CC=()
n=${#netcdf_fortran_LIBS[@]}
for ((i=0 ; i < n; ++i)) do
  case ${netcdf_fortran_LIBS[i]} in
    -Wl,-Wl,,-rpath,,*)
      netcdf_fortran_LIBS_FC+=("${netcdf_fortran_LIBS[i]}")
      netcdf_fortran_LIBS_CC+=("${netcdf_fortran_LIBS[i]/#-Wl,-Wl,,-rpath,,/-Wl,-rpath,}")
      unset "netcdf_fortran_LIBS[$i]"
      ;;
  esac
done
cdi_LIBS="${netcdf_fortran_LIBS[*]} -leccodes -lssl -lcrypto"
FCFLAGS="-g -O2 -Wc=$(command -v gcc) -Wc,-pipe -Wl,-pipe -Wc,-march=core-avx2 -Wc,-mtune=core-avx2 -Wl,-Wl,,--as-needed $($nf_config --fflags)"
CFLAGS="-g -O2 -march=core-avx2 -mtune=core-avx2 -pipe -Wl,--as-needed"
cdi_FCFLAGS="${FCFLAGS} -Wl,-Wl,,-rpath,,${eccodes_root}/lib64 ${netcdf_fortran_LIBS_FC[*]}"
if [[ ${libtype-shared} = static ]]; then
  # only a statically built ppm can link to the static netcdf_fortran
  # currently used
  ppm_configure="'FCFLAGS=$FCFLAGS ${netcdf_fortran_LIBS_FC[*]} -wmismatch=ppm_hex_checksum_f,mpi_allgather,mpi_allgatherv,mpi_allreduce,mpi_bcast,ppm_qsort_r,ppm_insertion_sort_once,ppm_heap_elem_increase_sort,ppm_heapify,ppm_heap_leaf_minimize,mpi_sendrecv,ppm_get_address,ppm_dist_mult_array_get_f2c' 'LIBS=${netcdf_fortran_LIBS[*]}'"
else
  ppm_configure="'FCFLAGS=$FCFLAGS -wmismatch=ppm_hex_checksum_f,mpi_allgather,mpi_allgatherv,mpi_allreduce,mpi_bcast,ppm_qsort_r,ppm_insertion_sort_once,ppm_heap_elem_increase_sort,ppm_heapify,ppm_heap_leaf_minimize,mpi_sendrecv,ppm_get_address,ppm_dist_mult_array_get_f2c'"
fi
${script_dir}build-cdi-pio-stack.sh \
  build="$build" \
  prefix="/work/k20200/$(id -un)/sw/%k/%v-%b" \
  multi_installs=: \
  builddir="$builddir" \
  CC_rpath_flag=-Wl,-rpath, \
  FC_rpath_flag=-Wl,-Wl,,-rpath,, \
  CPPFLAGS="-DNODEBUG $($nf_config --cflags) -I${eccodes_root}/include" \
  CFLAGS="$CFLAGS" \
  FCFLAGS="$FCFLAGS" \
  LDFLAGS="-L${eccodes_root}/lib64" \
  ppm_configure="$ppm_configure" \
  cdi_configure="'LIBS=$cdi_LIBS' 'FCFLAGS=$cdi_FCFLAGS' 'FFLAGS=$cdi_FCFLAGS' 'CFLAGS=${CFLAGS} -Wl,-rpath,${eccodes_root}/lib64 ${netcdf_fortran_LIBS_CC[*]}'" \
  SCRATCH="$SCRATCH" \
  --use-from-system libaec \
  --use-from-system hdf5 \
  --use-from-system pnetcdf \
  --use-from-system netcdf-c \
  --use-from-system netcdf-fortran \
  --use-from-system eccodes \
  "$@"
