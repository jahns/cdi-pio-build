SHELLCHECK = shellcheck

SCRIPTS = scripts/bash_functions scripts/create_srun_bcast.sh

check:
	$(SHELLCHECK) -x *.sh
	$(SHELLCHECK) -x $(SCRIPTS)

