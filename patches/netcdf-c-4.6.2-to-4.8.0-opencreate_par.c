/* netcdf-c 4.6.2 up to 4.8.0, when built with cmake, fail to call
 * MPI_Info_f2c here. That does not make a difference for mpich and
 * derived MPI implementations but is fatal in the case of OpenMPI.
 *
 * In self-defense this file provides a patch to insert in whatever
 * way is most convenient. Probably easiest is to compile once with
 * mpicc and add the resulting .o file to LIBS argument of netcdf-fortran
 * configure.
 */
#include <mpi.h>
#include <netcdf.h>
#include <netcdf_par.h>

int
nc_open_par_fortran(const char *path, int omode, MPI_Fint comm,
                    MPI_Fint info, int *ncidp)
{
  /* Convert fortran comm and info to C comm and info. */
  MPI_Comm comm_c = MPI_Comm_f2c(comm);
  MPI_Info info_c = MPI_Info_f2c(info);
  return nc_open_par(path, omode, comm_c, info_c, ncidp);
}

int
nc_create_par_fortran(const char *path, int cmode, MPI_Fint comm,
                      MPI_Fint info, int *ncidp)
{
  /* Convert fortran comm and info to C comm and info. */
  MPI_Comm comm_c = MPI_Comm_f2c(comm);
  MPI_Info info_c = MPI_Info_f2c(info);
  return nc_create_par(path, cmode, comm_c, info_c, ncidp);
}
