#! /bin/bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}scripts/bash_functions"

 module load cdt/21.09
 module swap cce cce/10.0.2
 module load CMake/3.22.1
 module load craype-haswell
 module load cray-hdf5-parallel/1.12.0.4 cray-netcdf-hdf5parallel/4.7.4.4

build="cce-${CRAY_CC_VERSION}-cray-mpich-${CRAY_MPICH_VERSION}"

setup_tracing_and_logging \
  "$HOME/cdi-pio-build-log$(date +%Y%m%dT%H%M%S)-${build}.txt"

# eccodes is built with gcc on this system but can still be used
# because only the serial API is called by current cdi version, hence
# set the paths manually
eccodes_root=/apps/daint/UES/jenkins/7.0.UP03/21.09/daint-gpu/software/ecCodes/2.23.0-CrayGNU-21.09
libaec_root=/apps/daint/UES/jenkins/7.0.UP03/21.09/daint-gpu/software/libaec/1.0.6-CrayGNU-21.09
jasper_root=/apps/daint/UES/jenkins/7.0.UP03/21.09/daint-gpu/software/JasPer/2.0.33-CrayGNU-21.09
jpeg_root=/apps/daint/UES/jenkins/7.0.UP03/21.09/daint-gpu/software/libjpeg-turbo/2.1.1-CrayGNU-21.09

#
prefix="/project/d56/tjahns/cdi-pio-installed"
SCRATCH="${SCRATCH}/cdi-pio-test-files-${HOSTNAME}-$$"
mkdir -p "${SCRATCH}"
lfs setstripe --stripe-count 8 "$SCRATCH"
builddir=$(mktemp -d "${XDG_RUNTIME_DIR}/cdi-pio-build-$(id -un)-XXXXXXX")

salloc="$(readlink -f ${script_dir}/scripts/salloc_retry) -t 00:30:00 -N 1 --exclusive -C gpu -A d56"
# debug partition only up on week days
if (($(date +%u) < 6)); then
  salloc+=' -p debug'
fi

"${script_dir}scripts/create_srun_bcast.sh" "${builddir}/bin" \
  /dev/shm "${builddir}"

trap build_cdipio_cleanup EXIT

${script_dir}build-cdi-pio-stack.sh \
  basedir=/project/d56/tjahns/cdi-pio-build \
  builddir="$builddir" \
  build="${build}" \
  SCRATCH="${SCRATCH}" \
  libtype=static \
  srcdir=/project/d56/tjahns/cdi-pio-build/src \
  prefix="${prefix}/%n-%b" \
  multi_installs=: \
  CC=cc FC=ftn FCFLAGS='-g -O2 -ef' CXX=CC F77=ftn \
  MPI_LAUNCH="$builddir/bin/srun" \
  --use-from-system libaec \
  "package_inst[libaec]=${libaec_root}" \
  --use-from-system eccodes \
  "package_inst[eccodes]=${eccodes_root}" \
  --use-from-system hdf5 \
  --use-from-system netcdf-c \
  --use-from-system netcdf-fortran \
  --use-from-system pnetcdf \
  yaxt_configure_env="${salloc}" \
  yaxt_check_env="${salloc}" \
  ppm_configure_env="${salloc}" \
  ppm_check_env="${salloc}" \
  cdi_configure_env="${salloc}" \
  cdi_configure="\"LDFLAGS=\$LDFLAGS -Wl,-rpath,${eccodes_root}/lib:${libaec_root}/lib:${jasper_root}/lib:${jpeg_root}/lib\" \"LIBS=\$LIBS -lsz -laec\"" \
  cdi_check_env="${salloc}" \
  "$@"
