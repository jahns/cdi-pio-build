#! /bin/bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}scripts/bash_functions"
module purge
module load git/2.31.1-gcc-11.2.0 \
  patch/2.7.6-gcc-11.2.0 \

compiler=nvhpc
module load ${compiler}/22.5-gcc-11.2.0 \
  openmpi/4.1.2-${compiler}-22.5


CC=mpicc
ompi_version=$(ompi_info --version | sed -n -e '1s/Open MPI v//;1p')
compiler_version=$($CC -V \
  | sed -n -e '/-bit target on /{' \
        -e 's/.* \([0-9][0-9.]*\).*[0-9][0-9]-bit target on .*/\1/;p;}')
build="${compiler}-${compiler_version}-ompi-${ompi_version}"

setup_tracing_and_logging \
  "$HOME/cdi-pio-build-log$(date +%Y%m%dT%H%M)-${build}.txt"
set -x
builddir=${builddir-$(mktemp -d "/dev/shm/cdi-pio-build-$(id -un)-XXXXXXX")}
# should be the following, but that directory is not currently available
SCRATCH="/scratch/${USER:0:1}/${USER}/cdi-pio-test-files-${build}"
mkdir -p "$SCRATCH"
lfs setstripe --stripe-count 8 "$SCRATCH"
trap build_cdipio_cleanup EXIT
BUILD_CDIPIO_KEEP_ARTIFACTS=true

spack_get_pkg_hash()
{
  local spec
  for spec in "$@" ; do
    spack find -l "${spec}%${compiler}@${compiler_version}" \
      | sed -n -e "/^[a-z0-9]* ${spec}@/{" -e "s/ *${spec}@.*//;p;}"
  done
}

spack_get_pkg_root()
{
  local pkg spec
  for pkg in "$@" ; do
    if [[ "$pkg" = /* ]]; then
      spec=$pkg
    else
      spec="${pkg}%${compiler}@${compiler_version}"
    fi
    spack find -p "${spec}" \
      | sed -n -e '/^-/d' -e 's/^[^@]*@[^ ]* *//;p'
  done
}

eccodes_hash=$(spack_get_pkg_hash eccodes)
eccodes_root=$(spack_get_pkg_root "/${eccodes_hash}")
declare -A eccodes_deps_by_hash eccodes_deps_by_pkg
# spack spec -l "/${eccodes_hash}" |
#   sed -n -E -e '/^[a-z0-9]{5,} *\^/p' -e '/^[a-z0-9]{5,} *^/{' -e 's/^([a-z0-9]{5,}) *^/\1/;p;}'
# exit 1
while read -r hash spec ; do
  eccodes_deps_by_hash[$hash]=$spec
  eccodes_deps_by_pkg[${spec%%@*}]=$hash
done < <(spack spec -l "/${eccodes_hash}" |
   sed -n -E -e '/^[a-z0-9]{5,} *\^/{' -e 's/^([a-z0-9]{5,}) *\^/\1 /;p;}')
libaec_hash=${eccodes_deps_by_pkg[libaec]}
printf 'Using libaec with spec %s\n' \
       "${eccodes_deps_by_hash[$libaec_hash]}"
libaec_root=$(spack_get_pkg_root "/$libaec_hash")
mpi_hash=$(command -v mpicc)
mpi_hash=${mpi_hash%/bin/mpicc}
mpi_hash=${mpi_hash##*/*-}
declare -A mpi_dependents
while read -r hash spec ; do
  mpi_dependents[$hash]=$spec
done < <(spack dependents -it "/${mpi_hash}" |
   sed -E -e '/^-/d')
# find netcdf_fortran etc. matching the selected mpi
netcdf_c_root=
netcdf_fortran_root=
for pkg in parallel-netcdf hdf5 netcdf-c netcdf-fortran ; do
  for pkg_hash in "${!mpi_dependents[@]}" ; do
    if [[ "${mpi_dependents[$pkg_hash]}" = ${pkg}* ]]; then
      pkg_root=$(spack_get_pkg_root "/$pkg_hash")
      typeset "${pkg/-/_}_root=$pkg_root"
      PATH="${pkg_root}/bin:$PATH"
      break
    fi
  done
done
nff_rpath="${netcdf_fortran_root}/lib"
nfc_rpath="${netcdf_c_root}/lib"
nff_libs="$(nf-config --flibs) -Wl,-rpath,${nff_rpath}"
COMMONFLAGS='-O2 -g -tp=zen3'
CFLAGS=${CFLAGS-$COMMONFLAGS}
CPPFLAGS=${CPPFLAGS-'-DNDEBUG'}
FCFLAGS=${FCFLAGS-$COMMONFLAGS}
LDFLAGS=${LDFLAGS--g -Wl,--as-needed}
OMPI_MCA_btl=^vader \
UCX_LOG_LEVEL=fatal \
OMPI_MCA_opal_warn_on_missing_libcuda=0 \
${script_dir}build-cdi-pio-stack.sh \
  build="${build}" \
  multi_installs=: \
  prefix="${HOME}/cdi-pio-stack/sw/%b/%k/%v" \
  basedir="$HOME/cdi-pio-build" \
  srcdir="$HOME/cdi-pio-stack/src" \
  archivedir="$HOME/cdi-pio-stack/archive" \
  builddir="$builddir" \
  CC=$CC FC=mpifort F77=mpifort CXX=mpiCC \
  CFLAGS="$CFLAGS" \
  CPPFLAGS="${CPPFLAGS}" \
  FCFLAGS="$FCFLAGS" \
  LDFLAGS="$LDFLAGS" \
  SCRATCH="$SCRATCH" \
  --use-from-system libaec \
  "package_inst[libaec]=${libaec_root}" \
  --use-from-system eccodes \
  "package_inst[eccodes]=${eccodes_root}" \
  --use-from-system hdf5 \
  "package_inst[hdf5]=${hdf5_root}" \
  --use-from-system netcdf-c \
  "package_inst[netcdf-c]=${netcdf_c_root}" \
  --use-from-system netcdf-fortran \
  "package_inst[netcdf-fortran]=${netcdf_fortran_root}" \
  --use-from-system pnetcdf \
  "package_inst[pnetcdf]=${parallel_netcdf_root}" \
  ppm_configure="CFLAGS='$CFLAGS -fopenmp' FCFLAGS='$FCFLAGS -fopenmp -I${netcdf_fortran_root}/include' LIBS='${nff_libs}'" \
  cdi_configure="CFLAGS='$CFLAGS' FCFLAGS='$FCFLAGS' LIBS='${nff_libs} $(nc-config --libs) -Wl,-rpath,${nfc_rpath}' LDFLAGS='$LDFLAGS -L${eccodes_root}/lib64 -Wl,-rpath,${eccodes_root}/lib64'" \
  "$@"

