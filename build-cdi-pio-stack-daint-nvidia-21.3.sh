#! /bin/bash
set -eu
script_dir=$(dirname "$0")
if [[ x$script_dir != x ]]; then
  script_dir+=/
fi
# shellcheck source=scripts/bash_functions
source "${script_dir}scripts/bash_functions"

 module load CMake/3.22.1
 module load craype-haswell
 module swap cce nvidia/21.3
 module swap PrgEnv-cray PrgEnv-nvidia/6.0.10
 module load cray-parallel-netcdf/1.12.1.4
 module load cray-hdf5-parallel/1.12.0.4
 module load cray-netcdf-hdf5parallel/4.7.4.4
#
#
build="nvidia-${NVIDIA_VERSION}-cray-mpich-${CRAY_MPICH_VERSION}"
#
setup_tracing_and_logging \
  "$HOME/cdi-pio-build-log$(date +%Y%m%dT%H%M%S)-${build}.txt"

# eccodes is built with gcc on this system but can still be used
# because only the serial API is called by current cdi version, hence
# set the paths manually
eccodes_root=/apps/daint/UES/jenkins/7.0.UP03/21.09/daint-gpu/software/ecCodes/2.23.0-CrayGNU-21.09
libaec_root=/apps/daint/UES/jenkins/7.0.UP03/21.09/daint-gpu/software/libaec/1.0.6-CrayGNU-21.09
jasper_root=/apps/daint/UES/jenkins/7.0.UP03/21.09/daint-gpu/software/JasPer/2.0.33-CrayGNU-21.09
jpeg_root=/apps/daint/UES/jenkins/7.0.UP03/21.09/daint-gpu/software/libjpeg-turbo/2.1.1-CrayGNU-21.09

prefix="/project/d56/tjahns/cdi-pio-installed"
SCRATCH="${SCRATCH}/cdi-pio-test-files-${HOSTNAME}-$$"
mkdir -p "${SCRATCH}"
lfs setstripe --stripe-count 8 "$SCRATCH"
builddir=$(mktemp -d "${XDG_RUNTIME_DIR}/cdi-pio-build-$(id -un)-XXXXXXX")
trap build_cdipio_cleanup EXIT

scratch_is_global=: \
  "${script_dir}scripts/create_srun_bcast.sh" "${builddir}/bin" \
  "${SCRATCH}" "\\(${XDG_RUNTIME_DIR}\\|${builddir}\\)"

salloc="$(readlink -f ${script_dir}/scripts/salloc_retry) -t 00:30:00 -N 1 --exclusive -C gpu -A d56"
# debug partition only up on week days
if (($(date +%u) < 6)); then
  salloc+=' -p debug'
fi

${script_dir}build-cdi-pio-stack.sh \
  basedir=/project/d56/tjahns/cdi-pio-build \
  build="$build" \
  builddir="$builddir" \
  SCRATCH="${SCRATCH}" \
  libtype=shared \
  prefix="${prefix}/%n-%b" \
  multi_installs=: \
  CC=cc CFLAGS='-g -O2 -tp=haswell-64' \
  FC=ftn FCFLAGS='-g -O2 -tp=haswell-64' CXX=CC F77=ftn \
  MPI_LAUNCH="$builddir/bin/srun" \
  CMAKE_EXTRA_ARGS=-DCMATH_LIBRARIES:FILEPATH=-lm \
  --use-from-system libaec \
  --use-from-system eccodes \
  --use-from-system hdf5 \
  --use-from-system netcdf-c \
  --use-from-system netcdf-fortran \
  --use-from-system pnetcdf \
  yaxt_configure_env="${salloc}" \
  yaxt_check_env="${salloc}" \
  ppm_configure_env="${salloc}" \
  ppm_check_env="${salloc}" \
  cdi_configure_env="${salloc}" \
  cdi_check_env="${salloc}" \
  cdi_configure="'--with-eccodes=${eccodes_root}' 'LIBS=-L${jpeg_root}/lib -ljpeg -L${libaec_root}/lib -laec' \"LDFLAGS=\$LDFLAGS -Wl,--as-needed -L${eccodes_root}/lib -Wl,-rpath,${eccodes_root}/lib:${jasper_root}/lib:${jpeg_root}/lib:${libaec_root}/lib\"" \
  "$@"
